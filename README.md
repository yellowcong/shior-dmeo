## 框架搭建


### Spring+ Spring MVC +Hibernate  +Shiro
项目地址 ：ssh_shiro <br/>
文章地址<br/>
http://blog.csdn.net/yelllowcong/article/details/77678726

### Spring+ Spring MVC  +Mybatis +Shiro
项目地址： ssm_shiro <br/>
文章地址<br/>
http://blog.csdn.net/yelllowcong/article/details/78074462

## mybatis-generator-gui
这个项目是一个生成Mybatis的配置文件工具<br/>
可以根据数据库的表，生成Mapper和Dao


## shiro cas登录
项目名称：test_cas<br/>

## shiro 学习
<font color="red">test项目的shiro配置是比较全的</font>
文章地址<br/>
http://blog.csdn.net/yelllowcong/article/category/7350728

### 将Session持久化到数据库
项目名 ：test<br/>
文章地址<br/>
http://blog.csdn.net/yelllowcong/article/details/78863232


### Shiro之保存Session到数据库中-yellowcong 
项目名 ：test<br/>
文章地址<br/>
http://blog.csdn.net/yelllowcong/article/details/78863232


### shiro拦截器使用
项目名 ：test<br/>
文章地址<br/>
http://blog.csdn.net/yelllowcong/article/details/78828233

### Shiro之多Realm的认证及认证策略-yellowcong 
项目名 ：test<br/>
文章地址<br/>
http://blog.csdn.net/yelllowcong/article/details/78827652
