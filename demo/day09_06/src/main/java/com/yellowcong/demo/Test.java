package com.yellowcong.demo;

import java.awt.Button;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.KeyboardFocusManager;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
*
*作者:yellowcong
*日期:2017/09/06
*時間:15:25:49
*描述:
*/
public class Test {
	public static void main(String[] args) {
		Demo demo = new Demo();
		
	}
}

class Demo extends Frame{
	
	//窗口的最小高
	public static final int MIN_DAILOG_HEIGHT = 200;
	
	//窗口的最小宽
	public static final int MIN_DAILOG_WIDTH = 300;
	
	private Button button  = new Button("button");
	/**
	 * @throws HeadlessException
	 */
	public Demo() throws HeadlessException {
		super();
		
		//设置可以见到
		super.setVisible(true);
		
		//获取电脑屏幕的宽度和高度	
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension dimension = kit.getScreenSize();
        int screenHeight = dimension.height;
        int screenWidth = dimension.width;
        
        //设定画面的宽度和高度
        super.setSize(screenWidth / 2, screenHeight / 2);
       
        //设定在窗体居中
        super.setLocationRelativeTo(null);
        
        //setLocationByPlatform(true);
       
        //Image img = kit.getImage(this.getClass().getResource("Nbrcats2_icon[1].gif"));
       
        //setIconImage(img);
        
        
		//添加组建
		super.add(button);
		
		//设定标题
		super.setTitle("yellowcong");
		
		//设定布局为空,下面的按钮位置才可以设定
		super.setLayout(null);
		
		//设定按钮的位置
		button.setBounds(1, 1, 300, 100);
		
		//设定按钮的监听事件
		button.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.out.println("点击了按钮");
			}
		});
		
		//设定窗口的监听事件
		super.addWindowListener(new WindowAdapter() {
			
			//点击 x 关闭系统,默认情况下是关闭不了的
			@Override
			public void windowClosing(WindowEvent e) {
				System.out.println("系统退退出");
				//退出系统
				System.exit(0);
			}
			
			
		});
		
		super.addComponentListener(new ComponentAdapter() {
			
			public void componentResized(ComponentEvent e) {
				// TODO Auto-generated method stub
				
//				e.getComponent().getWidth();  //直接获取窗口的大小
				Component dailog = e.getComponent();
				
				Dimension dimension = dailog.getSize();
		        int windowHeight = dimension.height;
		        int windowWidth = dimension.width;
		        
		        System.out.printf("更改窗口大小\t高%d,宽%d\r\n",windowHeight,windowWidth);
		        
		        //获取当前窗口
		        Window window = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusedWindow();
		        
		        if(windowHeight <MIN_DAILOG_HEIGHT ) {
		        	window.setSize(windowWidth, MIN_DAILOG_HEIGHT);
		        }
		        
		        if( windowWidth<MIN_DAILOG_WIDTH) {
		        	window.setSize(MIN_DAILOG_WIDTH, windowHeight);
		        }
			}
		});
		
		//设置是否可以更改窗口大小
		//super.setResizable(false);
	}
	
}
