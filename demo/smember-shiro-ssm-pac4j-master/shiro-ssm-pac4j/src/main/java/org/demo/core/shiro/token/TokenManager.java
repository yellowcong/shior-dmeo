package org.demo.core.shiro.token;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.demo.core.mybatis.model.User;

public class TokenManager {
    public static Subject getSubject(){
        Subject subject = SecurityUtils.getSubject();
        return subject;
    }
    
    public static User getPrincipal(){
        User user = (User) getSubject().getPrincipal();
        return user;
    }
    
    public static Session getSession(){
        Session session = getSubject().getSession();
        return session;
    }
    
    public static void logout(){
        getSubject().logout();
    }
}
