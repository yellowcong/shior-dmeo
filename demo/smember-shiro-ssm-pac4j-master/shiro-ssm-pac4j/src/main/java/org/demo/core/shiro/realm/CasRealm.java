package org.demo.core.shiro.realm;

import io.buji.pac4j.realm.Pac4jRealm;
import io.buji.pac4j.subject.Pac4jPrincipal;
import io.buji.pac4j.token.Pac4jToken;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.demo.common.Constant;
import org.demo.common.util.JsonUtils;
import org.demo.common.util.LoggerUtils;
import org.demo.common.util.PropertyUtils;
import org.demo.core.mybatis.model.Permission;
import org.demo.core.mybatis.model.Role;
import org.demo.core.mybatis.model.User;
import org.demo.core.shiro.token.TokenManager;
import org.demo.permission.service.PermissionService;
import org.demo.role.service.RoleService;
import org.pac4j.core.profile.CommonProfile;

import javax.annotation.Resource;
import java.util.*;
import java.util.Map.Entry;

@SuppressWarnings("all")
public class CasRealm extends Pac4jRealm {

    private final Class that = this.getClass();

    @Resource
    private RoleService roleService;
    @Resource
    private PermissionService permissionService;


    /**
     * @param authenticationToken
     * @return
     * @throws AuthenticationException
     * @Created by ZYJ on 2017年2月15日 上午11:30:56
     * @Version 1.0
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        final Pac4jToken token = (Pac4jToken) authenticationToken;
        final LinkedHashMap<String, CommonProfile> profiles = token.getProfiles();
        final Pac4jPrincipal principal = new Pac4jPrincipal(profiles);
        User user = new User();
        for (Entry<String, CommonProfile> entry : profiles.entrySet()) {
            String name = entry.getKey();
            LoggerUtils.fmtDebug(this.getClass(), "CasRealm 接受到 client名字为:%s", name);
            CommonProfile profile = entry.getValue();
            Map<String, Object> attributeMap = profile.getAttributes();
            String attributeJSON = JsonUtils.objToJson(attributeMap);
            user = JsonUtils.jsonToObjUseGson(attributeJSON, User.class);
        }
        return new SimpleAuthenticationInfo(user, profiles.hashCode(), getName());
    }

    /**
     * 获取权限信息, 页面上每次判断权限或角色时都将调用此方法<br>
     * session 中存储角色  roles<br>
     * 存储权限  permissions<br>
     * sesison 中保存权限信息过期时间毫秒数 permission_expire<br>
     * 过期后重新查询数据库更新权限信息. 过期时间设置5分钟<br>
     *
     * @param principals
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        final Set<String> roles = new HashSet<String>();
        final Set<String> permissions = new HashSet<String>();
        final SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        Object obj = principals.getPrimaryPrincipal();
        Session session = TokenManager.getSession();
        if (session == null) {
            return simpleAuthorizationInfo;
        }
        Long expire = (Long) session.getAttribute(Constant.SESSION_PERMISSION_EXPIRE_KEY);
        if (expire != null) {
            Long current = System.currentTimeMillis();
            boolean isNotExpire = current < expire;
            // 尚未过期
            if (isNotExpire) {
                Set<String> permissionSet = (Set<String>) session.getAttribute(Constant.SESSION_PERMISSION_CACHE_KEY);
                Set<String> roleSet = (Set<String>) session.getAttribute(Constant.SESSION_ROLE_CACHE_KEY);
                if (permissionSet != null && roleSet != null) {
                    LoggerUtils.debug(that, "检测到session中已经包含了权限信息内容, 并且未过期, 使用session缓存数据");
                    roles.addAll(roleSet);
                    permissions.addAll(permissionSet);
                    simpleAuthorizationInfo.addRoles(roles);
                    simpleAuthorizationInfo.addStringPermissions(permissions);
                    return simpleAuthorizationInfo;
                }
            }
        }
        LoggerUtils.debug(that, "Session中未检测到权限信息, 读取数据库并将权限信息缓存到Session中.");
        User user = (User) obj;
        // 如果 token 为空的, 直接返回空的授权4
        if (user == null || user.getId() == null) {
            return simpleAuthorizationInfo;
        }
        Set<Role> roleSet = roleService.queryByUserid(user.getId());
        for (Role role : roleSet) {
            roles.add(role.getCode());
        }
        if (roles == null || roles.isEmpty()) {
            return simpleAuthorizationInfo;
        }
        Set<Permission> permissionSet = permissionService.queryByRoleCode(roles);
        for (Permission permission : permissionSet) {
            permissions.add(permission.getCode());
        }
        simpleAuthorizationInfo.addRoles(roles);
        simpleAuthorizationInfo.addStringPermissions(permissions);
        // 缓存到Session中
        session.setAttribute(Constant.SESSION_PERMISSION_CACHE_KEY, permissions);
        session.setAttribute(Constant.SESSION_ROLE_CACHE_KEY, roles);
        // 过期日期毫秒数
        Long ex = System.currentTimeMillis() + Long.valueOf(PropertyUtils.getString("system.config.permission.cache.timeout"));
        session.setAttribute(Constant.SESSION_PERMISSION_EXPIRE_KEY, ex);
        return simpleAuthorizationInfo;
    }
}
