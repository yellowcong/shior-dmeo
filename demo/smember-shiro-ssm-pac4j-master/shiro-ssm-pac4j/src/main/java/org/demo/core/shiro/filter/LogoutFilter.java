package org.demo.core.shiro.filter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class LogoutFilter extends org.apache.shiro.web.filter.authc.LogoutFilter {

    // sso注销地址
    protected String logoutUrl;

    @Override
    protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
//        final String redirectUrl = getRedirectUrl();
//        URI uri = new URI(getLogoutUrl());
//        HttpClientBuilder builder = HttpClientBuilder.create();
//        HttpClient client = builder.build();
//        HttpPost post = new HttpPost();
//        post.setURI(uri);
//        HttpResponse httpResponse = client.execute(post);
//        Integer statusCode = httpResponse.getStatusLine().getStatusCode();//        boolean isSuccess = statusCode == HttpStatus.SC_OK;
//        if (isSuccess) {
//            issueRedirect(request, response, redirectUrl);
//        } else {
//            System.out.println("注销失败");
//        }
        super.preHandle(request, response);
        return false;
    }

    public String getLogoutUrl() {
        return logoutUrl;
    }

    public void setLogoutUrl(String logoutUrl) {
        this.logoutUrl = logoutUrl;
    }
}
