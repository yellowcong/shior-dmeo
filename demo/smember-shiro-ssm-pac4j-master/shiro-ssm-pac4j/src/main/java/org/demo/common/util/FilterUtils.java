package org.demo.common.util;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by NSTL on 2017/2/22.
 */
public class FilterUtils {

    /**
     * 判断请求是否是ajax请求
     *
     * @param request
     * @return
     */
    public static boolean isAjax(HttpServletRequest request) {
        Boolean isAjax = "XmlHttpRequest".equalsIgnoreCase(request.getHeader("X-Requested-With"));
        return isAjax;
    }

}
