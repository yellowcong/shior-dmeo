package org.demo.permission.service.impl;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;

import org.demo.core.mybatis.mapper.PermissionMapper;
import org.demo.core.mybatis.model.Permission;
import org.demo.permission.service.PermissionService;
import org.springframework.stereotype.Service;

/**
 * Created by NSTL on 2017/2/20.
 */
@Service
public class PermissionServiceImpl implements PermissionService{
    @Resource
    private PermissionMapper permissionMapper;


    @Override
    public Set<Permission> queryByRoleCode(String code) {
        Set<String> codes = new HashSet<String>();
        codes.add(code);
        Set<Permission> result = queryByRoleCode(codes);
        return result;
    }

    @Override
    public Set<Permission> queryByRoleCode(Set<String> code) {
        Set<Permission> result = permissionMapper.queryByRoleCode(code);
        return result;
    }

}
