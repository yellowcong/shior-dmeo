package org.demo.core.shiro.filter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.apache.shiro.web.util.WebUtils;

/**
 * 用于权限校验
 * Created by NSTL on 2017/2/21.
 */
public class PermissionFilter extends AccessControlFilter {
    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
        // 当shiroFilter的 filterChainDefinitions 中包含下面类似的配置时, mappedValue才会有值
        // /u/online=permission[/u/online]
        // /u/online=role[管理员]
        Subject subject = getSubject(request, response);
        if (mappedValue != null) {
            String[] arr = (String[]) mappedValue;
            for (String per : arr) {
                if (subject.isPermitted(per)) {
                    return true;
                }
            }
        }
        // 获取请求的路径
        HttpServletRequest r = (HttpServletRequest) request;
        String uri = r.getRequestURI();
        String basePath = r.getContextPath();
        if (basePath != null && !StringUtils.isBlank(basePath) && uri.startsWith(basePath)) {
            uri = uri.replace(basePath, "");
        }

        if (subject.isPermitted(uri)) {
            return true;
        }

        /*
判断是否ajax使用, 如果是ajax使用需要单独的返回, 否则ajax在页面上将没有任何放回值
if(FilterUtils.isAjax(request)){
LoggerUtils.fmtDebug(PermissionsFilter.class, "Is Ajax request.Request url is [%s]", uri);
Map<String, Object> resultMap = new HashMap<String, Object>();
resultMap.put("status", 300);
resultMap.put("message", "未授权");
FilterUtils.out(response, resultMap);
}
*/
        return false;
    }

    /**
     * 上面方法返回false时,会调用此方法
     *
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        // 权限不足, 重定向到错误页面
        WebUtils.issueRedirect(request, response, "/error/401");
        return false;
    }
}