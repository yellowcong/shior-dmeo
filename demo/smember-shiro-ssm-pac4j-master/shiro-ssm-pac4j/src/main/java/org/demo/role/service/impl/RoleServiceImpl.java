package org.demo.role.service.impl;

import java.util.Set;

import javax.annotation.Resource;

import org.demo.common.util.LoggerUtils;
import org.demo.core.mybatis.mapper.RoleMapper;
import org.demo.core.mybatis.model.Role;
import org.demo.role.service.RoleService;
import org.springframework.stereotype.Service;

/**
 * Created by NSTL on 2017/2/20.
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Resource
    private RoleMapper roleMapper;	
    @Override
    public Set<Role> queryByUserid(final Integer userid) {
        if (userid == null) {
            LoggerUtils.error(this.getClass(), "Illegal Argument [ userid ] is null.");
            return null;
        }
        Set<Role> roles =  roleMapper.queryByUserid(userid);
        roles = null == roles ? null : roles;
        return roles;
    }
}
