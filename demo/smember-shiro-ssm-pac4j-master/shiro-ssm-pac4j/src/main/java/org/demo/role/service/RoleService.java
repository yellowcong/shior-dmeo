package org.demo.role.service;

import java.util.Set;

import org.demo.core.mybatis.model.Role;

/**
 * Created by NSTL on 2017/2/20.
 */
public interface RoleService {
    /**
     * 根据 user 对象内不为null的字段查询
     *
     * @param userid
     * @return
     */
    Set<Role> queryByUserid(Integer userid);
}
