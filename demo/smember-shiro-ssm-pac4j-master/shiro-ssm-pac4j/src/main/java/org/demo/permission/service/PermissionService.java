package org.demo.permission.service;

import java.util.Set;

import org.demo.core.mybatis.model.Permission;

/**
 * Created by NSTL on 2017/2/20.
 */
public interface PermissionService {

    /**
     * 根据 role.code字段查询角色拥有的权限信息
     * @param code
     * @return
     */
    Set<Permission> queryByRoleCode(String code);

    /**
     * 查询多个 角色代码 包含的所有权限
     * @param code
     * @return
     */
    Set<Permission> queryByRoleCode(Set<String> code);


}
