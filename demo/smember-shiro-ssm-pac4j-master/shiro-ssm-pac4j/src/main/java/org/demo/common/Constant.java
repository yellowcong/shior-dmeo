package org.demo.common;

/**
 * Created by NSTL on 2017/2/20.
 */
public class Constant {

    /** Session 中存储权限信息过期时间的属性名字 **/
    public static final String SESSION_PERMISSION_EXPIRE_KEY = "permission_expire";
    /** Session 中缓存权限的key名字 **/
    public static final String SESSION_PERMISSION_CACHE_KEY = "permissions";
    /** Session 中缓存角色信息的key的名字 **/
    public static final String SESSION_ROLE_CACHE_KEY = "roles";

}
