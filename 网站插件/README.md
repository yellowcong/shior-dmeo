# shior-dmeo
这是一个学习shior的案例，大概会使用的是Spring+SpringMVC+Mybatis的框架来做

本教程是SSM（SpringMVC + Spring + Mybatis + Freemarker + JSP） + Shiro + Redis 做的整体Demo，其他框架需要自己自行解决，所以不做其他框架的讲解，其实是大同小异。

Shior 的简单案例 http://www.sojson.com/shiro