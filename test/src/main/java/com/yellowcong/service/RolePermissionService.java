package com.yellowcong.service;

import java.util.List;
import java.util.Set;

import com.yellowcong.shiro.dto.FilterInfo;

/**
 * 创建日期:2017/12/20 <br/>
 * 创建时间:14:24:43 <br/>
 * 创建用户:yellowcong <br/>
 * 功能描述:
 */
public interface RolePermissionService {
	/**
	 * 
	 * 创建日期:2017年9月23日<br/>
	 * 创建用户:yellowcong<br/>
	 * 功能描述:通过年龄，获取用户
	 * @return
	 */
	List<FilterInfo> list();
	
	/**
	 * 创建日期:2017/12/20<br/>
	 * 创建时间:16:11:02<br/>
	 * 创建用户:yellowcong<br/>
	 * 机能概要:通过用户角色来获取权限
	 * @param role
	 * @return
	 */
	Set<String> getPermissionByRole(Set<String> roles);
}
