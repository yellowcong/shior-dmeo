package com.yellowcong.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yellowcong.service.SessionService;
import com.yellowcong.shiro.dao.SessionMapper;
import com.yellowcong.shiro.model.Session;

/**
 * 创建日期:2017/12/21<br/>
 * 创建时间:8:56:09<br/>
 * 创建用户:yellowcong<br/>
 * 机能概要:
 */
@Service("sessionService")
public class SessionServiceImpl implements SessionService{
	
	@Autowired
	private SessionMapper sessionMapper ;
	
	public int insert(Session session) {
		return this.sessionMapper.insert(session.getId(),session.getSession());
	}

	public int delete(String sessionid) {
		return this.sessionMapper.delete(sessionid);
	}

	public int update(Session session) {
		return this.sessionMapper.update(session.getId(),session.getSession(),session.getUsername());
	}

	public Session load(String sessionid) {
		return this.sessionMapper.load(sessionid);
	}

}
