package com.yellowcong.service;

import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.stereotype.Service;

/**
 * 创建日期:2017/12/20<br/>
 * 创建时间:17:21:33<br/>
 * 创建用户:yellowcong<br/>
 * 机能概要:
 */
@Service("shiroService")
public class ShiroService {

	
	@RequiresRoles({"admin"})
	public void test() {
		System.out.println("执行了程序");
	}
}
