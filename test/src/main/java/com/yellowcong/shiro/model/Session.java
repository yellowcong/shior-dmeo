package com.yellowcong.shiro.model;

import java.io.Serializable;

/**
 * @author 
 */
public class Session implements Serializable {
    /**
     * Sessoin的id
     */
    private String id;

    /**
     * Session的序列化对象
     */
    private String session;
    
    /**
     * 用户名
     */
    private String username;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}