package com.yellowcong.shiro.model;

import java.io.Serializable;

/**
 * @author 
 */
public class RolePermission implements Serializable {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 关联角色
     */
    private Integer roleId;

    /**
     * 关联权限
     */
    private Integer permissionId;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }

}