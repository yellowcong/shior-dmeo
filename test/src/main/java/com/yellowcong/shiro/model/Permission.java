package com.yellowcong.shiro.model;

import java.io.Serializable;

/**
 * @author
 */
public class Permission implements Serializable {
	/**
	 * 主键
	 */
	private Integer id;

	/**
	 * 权限名称
	 */
	private String name;

	/**
	 * 权限
	 */
	private String url;

	private static final long serialVersionUID = 1L;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}