package com.yellowcong.controller;


import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.yellowcong.service.ShiroService;

/**
 * 创建日期:2017年9月23日 <br/>
 * 创建用户:yellowcong <br/>
 * 功能描述:用户控制成
 */
@Controller
@RequestMapping("/user")
@Scope("prototype")
public class UserController {
		
	@Autowired
	private ShiroService shiroService ;
	/**
	 * 跳转到登录页面
	 * 创建日期:2017年9月23日<br/>
	 * 创建用户:yellowcong<br/>
	 * 功能描述:
	 * @return
	 */
	@RequestMapping(value="/login",method=RequestMethod.GET)
	public String loginInput(){
		return "user/login";
	}
	
	/**
	 * 创建日期:2017年9月23日<br/>
	 * 创建用户:yellowcong<br/>
	 * 功能描述:用户登录操作
	 * @param username
	 * @param password
	 * @return
	 */
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public String login(String username,String password){
		try {
			Subject subject = SecurityUtils.getSubject();
			
			if(subject.isAuthenticated()){
				System.out.println("------------用户已经授权，已经登录-----------");
			}
			//用户登录
			UsernamePasswordToken token = new UsernamePasswordToken(username,password);
			//保存session
			token.setRememberMe(true);
			
			//登录用户
			subject.login(token);
			
			//redirect 方法返回的是
			return "redirect:/user/list";
		} catch (AuthenticationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "user/login";
		}
		
	}
	
	/**
	 * 退出系统
	 * @return
	 */
	@RequestMapping("/loginOut")
	public String loginOut(){
		//获取到Subject对象，然后退出
		SecurityUtils.getSubject().logout();
		return "redirect:/user/list";
	}
	
	@RequestMapping("/list")
	public String list(){
		return "user/list";
	}
	
	@RequestMapping("/admin")
	public String admin(){
		return "user/admin";
	}
	
	@RequestMapping("/user")
	public String user(){
		return "user/user";
	}
	/**
	 * 错误跳转页面
	 * @return
	 */
	@RequestMapping("/error")
	public String error(){
		return "user/error";
	}
	
	//只有管理员，才能执行这个方法
	@RequestMapping("/admin/test")
	public String adminTest(){
		shiroService.test();
		return "user/admin_test";
	}
	
}
