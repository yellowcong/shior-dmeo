package com.yellowcong.service.impl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.yellowcong.service.MessageService;
import com.yellowcong.shiro.model.Message;
import com.yellowcong.shiro.model.Session;

/**
 * 创建日期:2017年9月23日 <br/>
 * 创建用户:yellowcong <br/>
 * 功能描述:  
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:spring.xml")
public class TestMessageService{
	
	@Autowired
	MessageService messageService;
	
	@Test
	public void testGetMsgById() {
		Message msg = this.messageService.getMsg("SYSTEM.E.2","逗比","三炮");
		System.out.println(msg.getMessage());
	}
	
}
