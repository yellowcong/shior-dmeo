package com.yellowcong.shiro.filter;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.yellowcong.service.RolePermissionService;
import com.yellowcong.shiro.dto.FilterInfo;

/**
 * 创建日期:2017年12月17日
 * 创建时间:下午9:39:30
 * 创建用户:yellowcong
 * 机能概要:
 */
public class FilterChainDefinitionsMapBuilder {
	
	@Autowired
	RolePermissionService rolePermissionService;	
	
	//当数据配置是shiro的情况，就是系统配置，不做处理
	private static final String FILTER_TYPE = "shiro";
	/**
	 * 获取权限
	 * @return
	 */
	public LinkedHashMap<String, String> loadFilterChainDefinitions(){
		//用于存放认证的数据结果
		LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
		
		//获取权限配置
		List<FilterInfo>  lists = this.rolePermissionService.list();
		for(FilterInfo info:lists) {
			if(info.getType().equals(FILTER_TYPE)) {
				map.put(info.getUrl(), info.getName());
			}else {
				//比如是roles[admin]
				map.put(info.getUrl(),info.getType()+"["+info.getName()+"]");
			}
		}
		
		return map;
	}
}
