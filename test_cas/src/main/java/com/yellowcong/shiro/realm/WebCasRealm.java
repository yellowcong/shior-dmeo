package com.yellowcong.shiro.realm;

import java.util.Set;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.cas.CasRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import com.yellowcong.service.RolePermissionService;
import com.yellowcong.service.UserRoleService;
import com.yellowcong.service.UserService;
import com.yellowcong.shiro.model.User;

/**
 * 创建日期:2017/12/21<br/>
 * 创建时间:17:57:14<br/>
 * 创建用户:yellowcong<br/>
 * 机能概要:创建Cas的realm,需要继承CasRealm 类
 */
public class WebCasRealm extends CasRealm {

	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private RolePermissionService rolePermissionService;

	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		
		// 用户名称
		Object username = principals.getPrimaryPrincipal();

		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		// 根据用户ID查询角色（role），放入到Authorization里。
		Set<String> roles = userRoleService.getRoleByName(username.toString());
		info.setRoles(roles);

		// 根据用户ID查询权限（permission），放入到Authorization里。
		Set<String> permissions = null;
		if (roles.size() > 0) {
			permissions = this.rolePermissionService.getPermissionByRole(roles);
		}
		info.setStringPermissions(permissions);

		return info;
	}

	/**
	 * 
	 * 1、CAS认证 ,验证用户身份
	 * 2、将用户基本信息设置到会话中
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		
		//调用父类的方法，然后授权用户
		AuthenticationInfo authc = super.doGetAuthenticationInfo(token);
		
		//获取用户名
		String username = (String) authc.getPrincipals().getPrimaryPrincipal();
		
		//数据库中，查询用户的信息
		User user = userService.login(username);
				
		SecurityUtils.getSubject().getSession().setAttribute("user", user);
		
		return authc;
	}

}
