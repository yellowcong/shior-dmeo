package com.yellowcong.shiro.model;

import java.io.Serializable;

/**
 * @author 
 */
public class UserRole implements Serializable {
    /**
     * 主键
     */
    private Integer id;

    private Integer userId;

    private Integer roleId;
    
    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

}