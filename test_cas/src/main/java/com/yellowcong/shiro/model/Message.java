package com.yellowcong.shiro.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 
 */
public class Message implements Serializable {
	
	private static final long serialVersionUID = 1L;
    
	/**
     * 消息类型/E(ERROR)/I(INFO)/Q(QUESION)/S(SUCCESS)
     */
    private String messageKbn;

    /**
     * 机能名称/MESSAGE/USER/PASSAGE
     */
    private String kinoId;

    /**
     * 消息id/1.. 排序增长
     */
    private Byte messageId;

    /**
     * 消息内容
     */
    private String message;

    /**
     * 用户id
     */
    private String userid;

    /**
     * 用户名称
     */
    private String usernm;

    /**
     * 添加日期
     */
    private Date adddate;


    public String getMessageKbn() {
        return messageKbn;
    }

    public void setMessageKbn(String messageKbn) {
        this.messageKbn = messageKbn;
    }

    public String getKinoId() {
        return kinoId;
    }

    public void setKinoId(String kinoId) {
        this.kinoId = kinoId;
    }

    public Byte getMessageId() {
        return messageId;
    }

    public void setMessageId(Byte messageId) {
        this.messageId = messageId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsernm() {
        return usernm;
    }

    public void setUsernm(String usernm) {
        this.usernm = usernm;
    }

    public Date getAdddate() {
        return adddate;
    }

    public void setAdddate(Date adddate) {
        this.adddate = adddate;
    }
}