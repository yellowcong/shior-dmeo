package com.yellowcong.shiro.dto;

/**
 * 创建日期:2017/12/20
 * 创建时间:15:14:25
 * 创建用户:yellowcong
 * 机能概要:用于存储从数据库中查询的 shrio信息
 */
public class FilterInfo {
	
	//匹配的 对象
	private String name;
	
	//访问的url地址
	private String url;
	
	//配置的filter类型 shiro/roles
	//shiro表示是系统的配置
	//roles 表示用户管理，需要添加处理
	//user --> roles[user]
	private String type;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
