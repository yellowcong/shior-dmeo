package com.yellowcong.shiro.utils;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;

/**
 * 创建日期:2017年12月24日
 * 创建时间:上午9:54:32
 * 创建者    :yellowcong
 * 机能概要:用来写数据到客户端
 */
public class ControllerUtils {

	/**
	 * 创建日期:2017年12月24日<br/>
	 * 创建时间:上午9:55:02<br/>
	 * 创建用户:yellowcong<br/>
	 * 机能概要:在SpringMvc中获取到Session,写数据到服务器段
	 * @param response
	 * @param object
	 */
    public static void writeJSON(HttpServletResponse response,Object object){
        try {
            //设定编码 
            response.setCharacterEncoding("UTF-8");
            //表示是json类型的数据
            response.setContentType("application/json");
            //获取PrintWriter 往浏览器端写数据
            PrintWriter writer = response.getWriter();

            ObjectMapper mapper = new ObjectMapper(); //转换器
            //获取到转化后的JSON 数据
            String json = mapper.writeValueAsString(object);
            //写数据到浏览器
            writer.write(json);
            //刷新，表示全部写完，把缓存数据都刷出去
            writer.flush();

            //关闭writer
            writer.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
