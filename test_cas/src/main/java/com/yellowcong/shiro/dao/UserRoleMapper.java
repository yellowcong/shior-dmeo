package com.yellowcong.shiro.dao;

import java.util.Set;

/**
 * 创建日期:2017年9月23日 <br/>
 * 创建用户:yellowcong <br/>
 * 功能描述:用户的操作类
 */
public interface UserRoleMapper {
	/**
	 * 创建日期:2017/12/20
	 * 创建时间:15:53:08
	 * 创建用户:yellowcong
	 * 机能概要:通过用户名来获取角色
	 * @param username
	 * @return
	 */
	Set<String> getRoleByName(String username);
}
