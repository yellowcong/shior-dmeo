package com.yellowcong.shiro.dao;

import java.util.List;
import java.util.Set;

import org.apache.ibatis.annotations.Param;

import com.yellowcong.shiro.dto.FilterInfo;

/**
 * 创建日期:2017年9月23日 <br/>
 * 创建用户:yellowcong <br/>
 * 功能描述:用户的操作类
 */
public interface RolePermissionMapper {
	
	/**
	 * 创建日期:2017/12/20 <br/>
	 * 创建时间:14:15:40 <br/>
	 * 创建用户:yellowcong <br/>
	 * 功能描述:用来获取shiro的权限列表
	 */
	List<FilterInfo> list();
	
	
	/**
	 * 创建日期:2017/12/20<br/>
	 * 创建时间:16:11:02<br/>
	 * 创建用户:yellowcong<br/>
	 * 机能概要:通过用户角色来获取权限
	 * @param role
	 * @return
	 */
	Set<String> getPermissionByRole(@Param("roles") Set<String> roles);
	
}
