package com.yellowcong.shiro.dao;

import java.util.List;

import com.yellowcong.shiro.model.User;

/**
 * 创建日期:2017年9月23日 <br/>
 * 创建用户:yellowcong <br/>
 * 功能描述:用户的操作类
 */
public interface UserMapper {
	
	/**
	 * 
	 * 创建日期:2017年9月23日<br/>
	 * 创建用户:yellowcong<br/>
	 * 功能描述:来获取用户大于 这个 age的人
	 * @param age
	 * @return
	 */
	List<User> list();
	
	Integer getCount();
	/**
	 * 用户登录操作
	 * 创建日期:2017年9月23日<br/>
	 * 创建用户:yellowcong<br/>
	 * 功能描述:
	 * @param username 用户名
	 * @return
	 */
	User login(String username);
}
