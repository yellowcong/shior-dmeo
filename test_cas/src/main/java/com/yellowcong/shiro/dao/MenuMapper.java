package com.yellowcong.shiro.dao;

import java.util.List;

import com.yellowcong.shiro.model.Menu;

/**
 * 创建日期:2017/12/22<br/>
 * 创建时间:17:17:01<br/>
 * 创建用户:yellowcong<br/>
 * 机能概要:
 */
public interface MenuMapper {

	/**
	 * 创建日期:2017/12/22<br/>
	 * 创建时间:17:22:19<br/>
	 * 创建用户:yellowcong<br/>
	 * 机能概要:获取所有菜单
	 * @return
	 */
	List<Menu> list();
	
	/**
	 * 创建日期:2017/12/22<br/>
	 * 创建时间:17:22:59<br/>
	 * 创建用户:yellowcong<br/>
	 * 机能概要:插入菜单数据
	 * @param menu
	 * @return
	 */
	int insert(Menu menu);
}
