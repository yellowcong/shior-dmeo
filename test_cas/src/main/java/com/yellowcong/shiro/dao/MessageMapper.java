package com.yellowcong.shiro.dao;

import org.apache.ibatis.annotations.Param;

import com.yellowcong.shiro.model.Message;

/**
 * 创建日期:2017/12/22<br/>
 * 创建时间:17:17:01<br/>
 * 创建用户:yellowcong<br/>
 * 机能概要:
 */
public interface MessageMapper {
	
	/**
	 * 创建日期:2017年12月31日<br/>
	 * 创建时间:下午5:18:34<br/>
	 * 创建用户:yellowcong<br/>
	 * 机能概要:根据消息类型来获取id
	 * @param type 消息类型/E(ERROR)/I(INFO)/Q(QUESION)/S(SUCCESS)
	 * @param kinoId   机能名称/MESSAGE/USER/PASSAGE
	 * @param messageId 消息id 123
	 * @return
	 */
	Message getMsg(@Param("kionId")String kinoId,@Param("messageKbn") String messageKbn,@Param("messageId") String messageId);
}
