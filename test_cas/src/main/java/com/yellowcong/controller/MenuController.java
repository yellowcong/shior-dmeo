package com.yellowcong.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.druid.support.json.JSONUtils;
import com.yellowcong.service.MenuService;
import com.yellowcong.shiro.model.Menu;
import com.yellowcong.shiro.utils.ControllerUtils;

/**
 * 创建日期:2017/12/22<br/>
 * 创建时间:17:24:02<br/>
 * 创建用户:yellowcong<br/>
 * 机能概要:菜单处理类
 */
@Controller
@RequestMapping("/menu")
@Scope("prototype")
public class MenuController {
	
	@Autowired
	private MenuService menuService;
	
	/**
	 * 创建日期:2017年12月24日<br/>
	 * 创建时间:上午9:53:50<br/>
	 * 创建用户:yellowcong<br/>
	 * 机能概要:获取菜单列表
	 * @return
	 */
	@RequestMapping("/list")
	public void list(HttpServletResponse response) {
		//检索menu
		List<Menu> menus = this.menuService.list();
		ControllerUtils.writeJSON(response, menus);
	}
	
	@RequestMapping("/index")
	public String index(){
		return "/menu/index";
	}
}
