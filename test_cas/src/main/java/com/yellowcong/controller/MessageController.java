package com.yellowcong.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.yellowcong.service.MessageService;
import com.yellowcong.shiro.model.Message;
import com.yellowcong.shiro.utils.ControllerUtils;

/**
 * 创建日期:2017年12月31日<br/>
 * 创建时间:下午5:55:49 <br/>
 * 创建者 :yellowcong <br/>
 * 机能概要:
 */
@RequestMapping("/msg")
@Controller
@Scope("prototype")
public class MessageController {
	
	@Autowired
	private MessageService messageService;
	
	@RequestMapping("/getMsg")
	public void getMessage(String id,String args,HttpServletResponse response){
		String [] arrs = null;
		if(args != null && !StringUtils.isEmpty(args)){
			arrs = args.split(",");
		}
		
		//从数据库中获取数据
		Message msg = this.messageService.getMsg(id, arrs);
		
		//写数据到客户端
		Map<String,String> map = new HashMap<String, String>();
		map.put("type", msg.getMessageKbn());
		map.put("text", msg.getMessage());
		ControllerUtils.writeJSON(response, map);
	}
}
