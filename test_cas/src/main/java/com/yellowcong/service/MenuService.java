package com.yellowcong.service;

import java.util.List;

import com.yellowcong.shiro.model.Menu;

/**
 * 创建日期:2017/12/20 <br/>
 * 创建时间:14:24:43 <br/>
 * 创建用户:yellowcong <br/>
 * 功能描述:
 */
public interface MenuService {
	/**
	 * 
	 * 创建日期:2017年9月23日<br/>
	 * 创建用户:yellowcong<br/>
	 * 功能描述:通过年龄，获取用户
	 * @return
	 */
	List<Menu> list();
}
