package com.yellowcong.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yellowcong.service.MessageService;
import com.yellowcong.shiro.dao.MessageMapper;
import com.yellowcong.shiro.model.Message;

/**
 * 创建日期:2017年12月31日
 * 创建时间:下午5:25:25
 * 创建者    :yellowcong
 * 机能概要:
 */
@Service("messageService")
public class MessageServiceImpl implements MessageService{
	@Autowired
	private MessageMapper messageMapper;
	
	public Message getMsg(String id,String... args) {
		
		//MESSAGE.E.1 
		//获取Message机能中的 消息信息
		String [] ids = id.split("\\.");
		String kinoId = ids[0];
		String messageKbn = ids[1];
		String messageId = ids[2];
		
		Message msg = this.messageMapper.getMsg(kinoId,messageKbn, messageId);
		
		//当消息没有找到的情况
		if(msg == null){
			msg = new Message();
			msg.setMessageKbn("E");
			msg.setMessage(id+"没有找到，请确认数据库中存在此消息。");
			return msg;
		}
		
		//获取到msg之后 ，替换msg里面的匹配的数据
		//用户{0},您以登录。
		if(args != null && args.length >0){
			String messageStr = msg.getMessage();
			for(int i=0;i<args.length;i++){
				messageStr = messageStr.replace("{"+i+"}", args[i]);
			}
			msg.setMessage(messageStr);
		}
		return msg;
	}
}
