package com.yellowcong.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yellowcong.service.RolePermissionService;
import com.yellowcong.shiro.dao.PermissionMapper;
import com.yellowcong.shiro.dao.RolePermissionMapper;
import com.yellowcong.shiro.dto.FilterInfo;

/**
 * 创建日期:2017/12/20 <br/>
 * 创建时间:14:27:03 <br/>
 * 创建用户:yellowcong <br/>
 * 功能描述:
 */
@Service("rolePermissionService")
public class RolePermissionServiceImpl implements RolePermissionService{

	@Autowired
	private RolePermissionMapper rolePermissionMapper;
	
	public List<FilterInfo> list() {
		return this.rolePermissionMapper.list();
	}

	public Set<String> getPermissionByRole(Set<String> roles) {
		return this.rolePermissionMapper.getPermissionByRole(roles);
	}

}
