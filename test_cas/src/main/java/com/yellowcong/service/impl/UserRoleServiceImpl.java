package com.yellowcong.service.impl;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yellowcong.service.UserRoleService;
import com.yellowcong.shiro.dao.UserRoleMapper;

/**
 * 创建日期:2017/12/20 <br/>
 * 创建时间:14:27:03 <br/>
 * 创建用户:yellowcong <br/>
 * 功能描述:
 */
@Service("userRoleService")
public class UserRoleServiceImpl implements UserRoleService{

	@Autowired
	private UserRoleMapper userRoleMapper;
	
	public Set<String> getRoleByName(String username) {
		return userRoleMapper.getRoleByName(username);
	}

}
