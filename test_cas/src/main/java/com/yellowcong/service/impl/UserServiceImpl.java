package com.yellowcong.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yellowcong.service.UserService;
import com.yellowcong.shiro.dao.UserMapper;
import com.yellowcong.shiro.model.User;

/**
 * 创建日期:2017年9月23日 <br/>
 * 创建用户:yellowcong <br/>
 * 功能描述:  
 */
@Service("userService")
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserMapper userMapper;
	
	public List<User> list() {
		return userMapper.list();
	}

	public Integer getCount() {
		return this.userMapper.getCount();
	}

	public User login(String username) {
		return this.userMapper.login(username);
	}
}
