package com.yellowcong.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yellowcong.service.MenuService;
import com.yellowcong.shiro.dao.MenuMapper;
import com.yellowcong.shiro.model.Menu;

/**
 * 创建日期:2017年12月24日
 * 创建时间:上午9:50:00
 * 创建者    :yellowcong
 * 机能概要:
 */
@Service("menuService")
public class MenuServiceImpl implements MenuService{

	@Autowired
	private MenuMapper menuMapper;
	
	public List<Menu> list() {
		return this.menuMapper.list();
	}

}
