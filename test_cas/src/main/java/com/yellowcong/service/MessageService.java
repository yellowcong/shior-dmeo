package com.yellowcong.service;

import com.yellowcong.shiro.model.Message;

/**
 * 创建日期:2017年12月31日
 * 创建时间:下午5:24:34
 * 创建者    :yellowcong
 * 机能概要:
 */
public interface MessageService {
	
	/**
	 * 创建日期:2017年12月31日<br/>
	 * 创建时间:下午5:27:17<br/>
	 * 创建用户:yellowcong<br/>
	 * 机能概要:通过id来获取Message 
	 * @param id,例如  W.MESSAGE.1
	 * @param args 多个参数
	 * @return
	 */
	public Message getMsg(String id,String... args);
	
}
