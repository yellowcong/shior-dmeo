package com.yellowcong.core.mybatis;

import java.lang.reflect.ParameterizedType;

import org.mybatis.spring.support.SqlSessionDaoSupport;

/**
 * 创建日期:2017年9月23日 <br/>
 * 创建用户:yellowcong <br/>
 * 功能描述:Mybatis的工具包
 */          
public class BaseMybatisDao<T> extends SqlSessionDaoSupport{
	private Class<T> clazz;
	
	/**
	 * 获取当前泛型对象的数据类型
	 * @return
	 */
	@SuppressWarnings({  "unchecked" })
	private Class<T> getClazz() {
		if (this.clazz == null) {
			ParameterizedType type = (ParameterizedType) getClass().getGenericSuperclass();
			this.clazz = ((Class) type.getActualTypeArguments()[0]);
		}
		return this.clazz;
	}
	
}
