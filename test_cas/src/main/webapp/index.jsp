<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- 导入共同的文件 -->
<jsp:include page="/resources/page/jquery.jsp"></jsp:include>

<!-- 导入这个页面的样式文件 -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/index/index.css">

<!-- 导入这个页面的js文件 -->
<script src="<%=request.getContextPath()%>/resources/js/index/index.js"></script>

<title>yellowcongの后台管理</title>
</head>
<body class="layui-layout-body">
	<div class="layui-layout layui-layout-admin">
	
	<!-- 导入模板 -->	
	<jsp:include page="/resources/page/admin/top_menu.jsp" flush="true"/>
	
	<div id="index_content"></div>
		
	</div>
</body>
</html>

