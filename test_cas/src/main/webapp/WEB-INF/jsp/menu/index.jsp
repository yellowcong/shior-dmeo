<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/menu/index.js"></script>
</head>
<body class="layui-layout-body">
<div id="menuData">  
    <table id="gridMenuData"></table>  
    <div id="jqGridPager"></div>  
</div> 
</body>
</html>

