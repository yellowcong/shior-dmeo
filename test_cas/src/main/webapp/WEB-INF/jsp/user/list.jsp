<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
<%--shiro 标签 --%>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>  
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <title>后台管理 - yellowcong</title>
  <script src="<%=request.getContextPath() %>/resources/plugins/jquery/jquery.min.js"></script>
</head>
<body class="layui-layout-body"  >

<shiro:hasRole name="admin">
<a href="<%=request.getContextPath() %>/user/admin">管理员</a><br/>
</shiro:hasRole>

<shiro:hasRole name="user">
<a href="<%=request.getContextPath() %>/user/user">普通用户</a><br/>
</shiro:hasRole>

<a href="<%=request.getContextPath() %>/user/loginOut">退出</a><br/>

<h2>Admin才可以执行</h2>
<a href="<%=request.getContextPath() %>/user/admin/test">admin执行</a><br/>

</body>
</html>