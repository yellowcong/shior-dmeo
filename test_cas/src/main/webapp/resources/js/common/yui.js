//下面的是基于  yui的
var yui = new Object();
//打开的最大tab数目
var MAX_TAB_LENGTH = 6;
//服务器端,获取消息的url
var MESSAGE_URL = utils.contextPath() + "/msg/getMsg";
//菜单的url地址
var MENUS_URL = utils.contextPath() + "/menu/list";
//底部导航栏目的初始化信息
var FOOT_HTML = "© yellowcong.com - Shiro权限管理DEMO";

//-------------------------------------------------------------------------------------------
//初始化 yui
//-------------------------------------------------------------------------------------------
yui.init = function(){
	//初始化底部导航栏
	yui.initFoot(FOOT_HTML);
	
	//设定tab的样式
	yui.initMenu();
	
	//获取菜单数据
	//这个地方，可以改为本地的json数据
	var menus = yui.getMenuData();
	
	//数据个格式必须满足下面的要求，不然就设置不上去
/*	menus =  [{"menuId":"01","name":"个人中心","parent":"null","url":null,"menuLeve":1},
        {"menuId":"0101","name":"个人资料","parent":"01","url":"/user/info","menuLeve":2},
        {"menuId":"0102","name":"资料修改","parent":"01","url":"/user/update","menuLeve":2},
        {"menuId":"0103","name":"密码修改","parent":"01","url":null,"menuLeve":2},
        {"menuId":"02","name":"用户中心","parent":"null","url":null,"menuLeve":1},
        {"menuId":"0201","name":"用户列表","parent":"02","url":null,"menuLeve":2},
        {"menuId":"0202","name":"在线用户","parent":"02","url":null,"menuLeve":2},
        {"menuId":"04","name":"系统管理","parent":"null","url":null,"menuLeve":1},
        {"menuId":"0401","name":"菜单列表","parent":"04","url":"/menu/index","menuLeve":2},
        {"menuId":"0402","name":"菜单分配","parent":"04","url":null,"menuLeve":2}
       ];*/
	
	//设定菜单
	yui.setMenus(menus);

	//JavaScript代码区域
	layui.use('element', function() {
		//初始化tab页面
		yui.initTab("index_content");
	});
}
//-------------------------------------------------------------------------------------------
//初始化底部导航栏目
//参数
//content 表示
//例子：
//yui.initFoot("© yellowcong.com - Shiro权限管理");
//-------------------------------------------------------------------------------------------
yui.initFoot = function(content){
	$("body .layui-layout-admin").append('<div class="layui-footer">'+content+'</div>');
}
//-------------------------------------------------------------------------------------------
//设定点击菜单按钮的事件
//-------------------------------------------------------------------------------------------
yui.setClickFunc = function(){
	$(".layui-nav-child a").click(function(e) {
		// 阻止默认的点击事件
		e.preventDefault();
		var defaultUrl = "javascript:;";
		
		var _url = $(this);
		
		// 获取当前链接的url
		var url = _url.attr("href");
		
		var tabId = _url.attr("id").replace("_menu","");
		//tab文档的title
		var title =_url.html();
		
		//当存在node的情况下，选中节点，而不是再创建节点
		if($("#"+tabId).length >0){
			$("#"+tabId).trigger('click');	
			return ;
		}
		
		//判断tab的条数是否满足要求
		if(!yui.chkTabCnt()){
			return;
		}
		
		//当是默认的情况下
		var htmlStr ="";
		if (url == defaultUrl) {
			htmlStr = yui.createBlockHtml("没有添加这个功能");
			yui.createTab(tabId,title,htmlStr);
		} else {
			//加载页面，到body_content 元素中
			yui.loading(true);
			$("#body_content").load(url,function(response,status,xhr){
				 yui.loading(false);
				 if(status == "error"){
					 htmlStr =  yui.createBlockHtml(url+"加载失败");
					 yui.createTab(tabId,title,htmlStr);
				 }else{
					 htmlStr = $("#body_content").html();
					 yui.createTab(tabId,title,htmlStr);
					 //清空容器
					 $("#body_content").html("");
				 }
			});
		}
	});
}
//-------------------------------------------------------------------------------------------
//初始化菜单按钮
//-------------------------------------------------------------------------------------------
yui.initMenu = function(){
	var menuHtmlStr = 
	'<div class="layui-side layui-bg-black" style="margin-top: 60px;">'+
	'	<div class="layui-side-scroll">'+
	'		<!-- 左侧导航区域（可配合layui已有的垂直导航） -->'+
	'		<ul class="layui-nav layui-nav-tree" lay-filter="test" id="menu_left">'+
	'		</ul>'+
	'	</div>'+
	'</div>';
	
	$("body").append(menuHtmlStr);
}
//-------------------------------------------------------------------------------------------
//获取菜单的数据
//返回值：JSON
//-------------------------------------------------------------------------------------------
yui.getMenuData = function()  {
	var menus = null;

	// menus的 url
	var url = MENUS_URL;
	$.ajax({
		type : "get",// 使用提交的方法 post、get
		url : url,// 提交的地址
		data : null,// 数据
		async : false, // 配置是否异步操作
		dataType : "json",// 返回数据类型的格式
	}).done(function(result) {
		menus = result;
	});
	return menus;
}
//-------------------------------------------------------------------------------------------
//设定菜单的内容
//menus 菜单数组
//例子
//yui.setMenus(
//[{"menuId":"01","name":"个人中心","parent":"null","url":null,"menuLeve":1},
//{"menuId":"0101","name":"个人资料","parent":"01","url":"/user/info","menuLeve":2},
//{"menuId":"0102","name":"资料修改","parent":"01","url":"/user/update","menuLeve":2},
//{"menuId":"0103","name":"密码修改","parent":"01","url":null,"menuLeve":2},
//{"menuId":"02","name":"用户中心","parent":"null","url":null,"menuLeve":1},
//{"menuId":"0201","name":"用户列表","parent":"02","url":null,"menuLeve":2},
//{"menuId":"0202","name":"在线用户","parent":"02","url":null,"menuLeve":2},
//{"menuId":"04","name":"系统管理","parent":"null","url":null,"menuLeve":1},
//{"menuId":"0401","name":"菜单列表","parent":"04","url":"/menu/index","menuLeve":2},
//{"menuId":"0402","name":"菜单分配","parent":"04","url":null,"menuLeve":2}
//]);
//-------------------------------------------------------------------------------------------
yui.setMenus = function(menus){
	var htmlStr = "";
	// 父类菜单
	for (var i = 0; i < menus.length; i++) {
		// 获取菜单
		var menu = menus[i];

		// 菜单
		var name = menu.name;

		var id = utils.uuid()+"_menu";
		if (menu.menuLeve == 1) {
			if (i == 0) {
				// 父菜单
				htmlStr += '<li class="layui-nav-item layui-nav-itemed"><a class="" href="javascript:;">'
						+ name + '</a><dl class="layui-nav-child">';
			} else {
				htmlStr += '</dl></li><li class="layui-nav-item"><a class="" href="javascript:;" >'
						+ name + '</a><dl class="layui-nav-child">';
			}
		} else if (menu.menuLeve == 2) {
			var url = "javascript:;";
			if (menu.url != null) {
				url = utils.contextPath() + menu.url;
			}
			
			// 子菜单
			htmlStr += '<dd><a href="' + url + '" id="'+id+'">' + name + '</a></dd>';
		}
	}

	htmlStr += "</dl></li>";

	// 设定生成的html
	$("#menu_left").html(htmlStr);
	
	//设定菜单按钮后的事件
	yui.setClickFunc();
}
//-------------------------------------------------------------------------------------------
//创建等待的效果
//show 显示还是关闭
//yui.loading(true); //开启回话
//yui.loading(false); //关闭回话
//-------------------------------------------------------------------------------------------
yui.loading = function(show){
	if(show){
		//显示加载样式
		layer.load(3, {shade: [0.2, '#393D49']});
	}else{
		//关闭显示的效果
		var idStr = $(".layui-layer-loading").attr("id");
		if(idStr != undefined){
			idStr = idStr.replace("layui-layer","");
			//关闭显示的效果
			layer.close(idStr);
		}
	}
}

//**********************************message begin********************************************
//关闭message对话框
//-------------------------------------------------------------------------------------------
yui.closeMessage = function(){
	//message_id
	//type 默认dailog的模式
	var idStr = $(".layui-layer-dialog:last").attr("id");
	//type 1  page
//	var idStr = $(".layui-layer-page:last").attr("id");
	
	if(idStr != undefined){
		idStr = idStr.replace("layui-layer","");
		//关闭显示的效果
		layer.close(idStr);
	}
}
//-------------------------------------------------------------------------------------------
//获取服务器短的消息
//id 消息的id SYSTEM.I.1
//args消息模板匹配  单个消息 ,直接 传递 字符串，多个消息传递数组
//btns 调用的按钮
//例子
//var btns = [
//	{"确认":function(){ console.log("确认..."); yui.closeMessage()} },
//	{"取消":function(){console.log("取消...");yui.closeMessage()}}
//];
//yui.showMessage("SYSTEM.I.1",["doubi","2"],btns);
//-------------------------------------------------------------------------------------------
yui.showMessage = function(id,args,btns){
	if(args == null|| args == undefined){
		args = null;
	}else if(utils.isArray(args)){
		//当是数组的时候，直接加入 逗号进行分割
		args = args.join(",");
	}
	var data = {id:id,args:args};
	// menus的 url
	var url = MESSAGE_URL;
	$.ajax({
		type : "get",     // 使用提交的方法 post、get
		url : url,        // 提交的地址
		data : data,      // 数据
		async : false,    // 配置是否异步操作
		dataType : "json",// 返回数据类型的格式
	}).done(function(result) {
		//获取code 的id
		var  iconCd = getIconCode(result.type);
		
		var shadeClose =getShadeInfo();
		//消息框的
		var msgInfo = {title: "系统提示",icon:iconCd,shadeClose: shadeClose,content: result.text};
		
		var btn = new Array();
		if(btns != null && btns != undefined && btns.length >0){
			for(var i=0;i<btns.length;i++){
				var btnNow = btns[i];
				for(var key in btnNow){
					//添加buttons
					btn.push(key);
					if(i ==0){
						//第一个方法，必须是yes
						msgInfo.yes = btnNow[key];
					}else{
						//第二个或多个buttons的情况，需要btn2之后算 了
						msgInfo["btn"+(i+1)] = btnNow[key];
					}
				}
			}
			//设定 对话框
			msgInfo.btn = btn;
		}
		//打开对话框
		layer.open(msgInfo);
	});
}
//获取对话框关闭的方式，如果是多个按钮需要点击一个才可以关闭，如果是单个按钮的情况，就需要点击阴影关闭
function getShadeInfo(btns){
	var shadeClose = true; 
	if(btns != null && btns != undefined && btns.length >1){
		shadeClose = false;
	}
	return shadeClose;
}
//获取icon的类型
function getIconCode(type){
	//icon 类型
	//1 --> SUCESS
	//2 --> EROR
	//3 --> Question
	//7 --> INFO
	var iconCode = -1;
	switch(type){
	case "S":
		iconCode = 1;
		break;
	case "E":
		iconCode = 2;
		break;
	case "Q":
		iconCode = 3;
		break;
	case "I":
		iconCode = 7;
		break;
	}
	return iconCode;
}
//**********************************message end

//**********************************tab begin************************************************
//初始化tab页面用
//案例:
//yui.initTab('index_content');
//-------------------------------------------------------------------------------------------
yui.initTab = function(id){
	//body_content 用于存放 html加载的数据
	//content_tab 用于存放tab的名称
	//tab_content 存放 tab内容的
	
	var tabHtmlStr = 
		'<div class="layui-body">                                                      '+ 
		'		<div id="body_content" style="display: none;"></div>'+
		'		<div class="layui-tab" lay-filter="content_tab" lay-allowclose="true">'+
		'		  <ul class="layui-tab-title" id="tab_menus" style="-webkit-user-select:none; -moz-user-select:none;-ms-user-select:none;user-select:none;">'+
		'		    <li class="layui-this" lay-id="11">首页</li>'+
		'		  </ul>'+
		'		  <div class="layui-tab-content" id="tab_content">'+
		'		    <div class="layui-tab-item layui-show">首页内容</div>'+
		'		  </div>'+
		'		</div>'+
		'	</div>';
	//设定html
	$("#"+id).html(tabHtmlStr);
	
	//去掉第一个关闭按钮
	removeColose();
}
//删除第一个关闭的按钮
function removeColose(){
	$("#tab_menus li:first i").remove()
}
//-------------------------------------------------------------------------------------------
//创建tab页面
//id li标签的id
//title tab页面的标题
//htmlStr tab页面的内容
//-------------------------------------------------------------------------------------------
yui.createTab = function (id,title,htmlStr){
	// 后面添加
	$("#tab_menus li:last").after('<li id="'+id+'" onclick="yui.clickTab()">'+title+ '<i class="layui-icon layui-unselect layui-tab-close" onclick="yui.closeTab(\''+id+'\')">ဆ</i></li>');

	//添加tab页面里面的数据
	$("#tab_content .layui-tab-item:last").after('<div id="content_'+id+'" class="layui-tab-item">'+htmlStr+'</div>');

	//设定选中最后一个按钮
	$("#"+id).trigger("click");
}
//-------------------------------------------------------------------------------------------
//判断是否存在这个tab页面
//title 标题
//例子 yui.getTabByTitle("用户列表");
//返回值 boolean
//-------------------------------------------------------------------------------------------
yui.getTabByTitle = function(title){
	var id = "";
	//遍历，查看是否存在节点
	$("#tab_menus li").each(function(){
		if($(this).html().indexOf(title) > -1){
			id= $(this).attr("id");
			return ;
		}
	});
	return id;
}
//-------------------------------------------------------------------------------------------
//点击查看tab页面
//-------------------------------------------------------------------------------------------
yui.clickTab = function(){
	var title_id = $(this).attr("id");
	//获取内容体的id
	var content_id = "content_"+title_id;
	//添加显示的样式
	$(content_id).addClass("layui-show");
}
//-------------------------------------------------------------------------------------------
//获取打开的窗口数量  
//返回值为boolean
//-------------------------------------------------------------------------------------------
yui.chkTabCnt = function(){
	var tab_cnt = $("#tab_menus li").length;
	//当tab页中不包含这个
	if(tab_cnt >= MAX_TAB_LENGTH){
		//只有在阴影的情况下，才可以是点击阴影消失
		yui.showMessage("SYSTEM.I.1");
		return false;
	}
	return true;
}
//-------------------------------------------------------------------------------------------
//关闭tab页面
//tab_id 是tab的id
//-------------------------------------------------------------------------------------------
yui.closeTab = function(tab_id) {
	//获取内容id
	var content_id = $("#tab_content div.layui-show").attr("id");
	
	//判断是否是同一个tab页面的
	//是否选中了tab页面后，选择的删除
	if(tab_id != content_id.split("_")[1]){
		//
		//删除节点
		$("#"+tab_id).remove();
		$("#"+"content_"+tab_id).remove();
		return;
	}
	//设定选中节点，如果右边没有，就会选中左边的条目
	//选中前一个节点
	if($("#"+tab_id).next().length == 1){
		$("#"+tab_id).next().trigger('click');
	}else{
		$("#"+tab_id).prev().trigger('click');
	}
	
	//删除节点
	$("#"+tab_id).remove();
	$("#"+content_id).remove();
}

//**********************************tab end

//**********************************blockquote begin************************************************

//-------------------------------------------------------------------------------------------
//创建代码提示
//返回值: html 
//-------------------------------------------------------------------------------------------
yui.createBlockHtml = function(content) {
	// 提示内容
	htmlStr = '<blockquote class="layui-elem-quote layui-text">' + content + '</blockquote>';
	return htmlStr;
}
//-------------------------------------------------------------------------------------------
//设定数据
//title 标题
//content 内容
//返回值  :html
//-------------------------------------------------------------------------------------------
yui.showInfo = function( title, content) {
	// 提示标题
	var htmlStr = '<fieldset class="layui-elem-field layui-field-title" style=""><legend>'
			+ title + '</legend></fieldset>';
	// 提示内容
	htmlStr += '<blockquote class="layui-elem-quote layui-text">' + content + '</blockquote>';
	return htmlStr;
}

//**********************************blockquote end