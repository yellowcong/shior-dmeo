//-------------------------------------------------------------------------------------------
//菜单列表开发
//-------------------------------------------------------------------------------------------
$(function() {
	initForm();
});
function initForm(){
	//创建grid
	createMenuGrid ();
	setGridData();
}
//设定grid数据
function setGridData(){
	var data = getMenuData();
	//设定grid的数据
	$("#gridMenu")[0].addJSONData(data);
}
// -------------------------------------------------------------------------------------------
// 创建grid
// -------------------------------------------------------------------------------------------
function createMenuGrid() {
	var colNames = [ "菜单名称", "链接地址", "父类菜单", ];

	var colModel = [
		 {name:"name",sortable:true, width:200,editable: true, edittype: "text",  hidden:false},
		 {name:"url",sortable:true, width:200,editable: true, edittype: "text",  hidden:false},
		 {name:"parent",sortable:true, width:200,editable: true, edittype: "text", hidden:false}
	];
	dataList =  [];
	
	$("#gbox_gridMenuData").remove();
	$("#menuData").append("<table id='gridMenu'></table>");
	$("#gridMenu").jqGrid({
		data : [],
		datatype : "local",
		multiboxonly : true,
		cellEdit:true,
		cellsubmit : 'clientArray',
		colNames : colNames,
		colModel : colModel,
		width : 800,
		height : 400,
		scrollOffset : 0,
		shrinkToFit:true,
		rowNum : dataList.length,
		regional : 'ja',
		beforeSelectRow : function(rowid, event) {
		}
	});
	
	//修改表头数据
	
}
//获取菜单的数据
function getMenuData(){
	var menus = null;
	
	//menus的 url
	var url = utils.contextPath()+"/menu/list";
	$.ajax({  
        type : "get",  //使用提交的方法 post、get
        url : url,   //提交的地址
        data :null,  //数据
        async : false,   //配置是否异步操作
        dataType:"json",//返回数据类型的格式
	}).done(function(result){
		menus = result;
	});
	return menus;
}
