<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- layui的js -->
<script src="<%=request.getContextPath()%>/resources/plugins/layui/layui.js"></script>
<script src="<%=request.getContextPath()%>/resources/plugins/layer/layer.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/plugins/layui/css/layui.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/plugins/layer/theme/default/layer.css">
<!-- 导入了 layui 的样式-->

<!-- jquery的js -->
<script src="<%=request.getContextPath()%>/resources/plugins/jquery/jquery-1.7.1.js"></script>

<!-- Jgrid 开始-->
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/plugins/jqgrid/css/ui.jqgrid.css" />

<!-- 在jqgrid/js/i18n下还有其他的多语言包，可以尝试更换看效果 -->
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/plugins/jqgrid/js/i18n/grid.locale-cn.js"></script>
<!-- jqGrid插件包-必要 -->
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/plugins/jqgrid/js/jquery.jqGrid.min.js"></script>

<!-- jqGrid主题包-非必要 --> 
<!-- 在jqgrid/css/css这个目录下还有其他的主题包，可以尝试更换看效果 -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/plugins/jquery-ui-1.12.1.custom/jquery-ui.theme.css" />

<!-- 共通样式 -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/common/common.css">
<!-- jqgrid结束 -->

<!-- 工具类 -->
<script src="<%=request.getContextPath()%>/resources/js/common/utils.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/common/yui.js"></script>
<!-- 导入共通处理的js -->
<script src="<%=request.getContextPath()%>/resources/js/common/common.js"></script>


	