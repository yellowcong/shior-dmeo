package com.yellowcong.test;

import java.io.File;

import org.junit.Test;

public class WaterMarkUtilsTest {
	
	/**
	 * 测试单个水印的操作
	 */
	@Test
	public void testWarterCode(){
		String file = "C:/Users/yellowcong/Desktop/qr/logo/logo.jpg";
		//表示只写一个水印的操作
		WaterMarkUtils.textWaterMark(new File(file));
		//第二种写法
//		WaterMarkUtils.textWaterMark(new File(file),false);
		
	}
	
	/**
	 * 测试多水印的操作
	 * @throws Exception
	 */
	@Test
	public void testMoreTextWaterMark() throws Exception{
		String file = "C:/Users/yellowcong/Desktop/qr/logo/logo.jpg";
		WaterMarkUtils.textWaterMark(new File(file),true);
	}
	
	
	@Test
	public void testImageWaterMark() throws Exception{
		String file = "C:/Users/yellowcong/Desktop/qr/logo/logo.jpg";
		WaterMarkUtils.imageWaterMark(new File(file));
	}
	/**
	 * 多水印图片操作
	 * @throws Exception
	 */
	@Test
	public void testMoreImageWaterMark() throws Exception{
		String file = "C:/Users/yellowcong/Desktop/qr/logo/logo.jpg";
		WaterMarkUtils.imageWaterMark(new File(file),true);
	}
	
}
