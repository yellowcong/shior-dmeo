package com.yellowcong.test;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Random;

import org.junit.Test;

import com.yellowcong.test.ColorUtils.Colors;

public class QRUtilsTest2 {
	
	/**
	 * 不带logo的二维码
	 * @throws Exception
	 */
	@Test
	public void testWriteQRImg() throws Exception{
		String outPath = "C:/Users/yellowcong/Desktop/qr/test";
		QRCodeUtils.writeQRImg("http://baidu.com", outPath);
	}
	 
	/**
	 * 带logo的二维码
	 * @throws Exception
	 */
	@Test
	public void testWriteQRImgWithLogo() throws Exception{
		String logoPath = "C:/Users/yellowcong/Desktop/qr/logo/logo.jpg";
		String outPath = "C:/Users/yellowcong/Desktop/qr/test";
		QRCodeUtils.writeQRImg("http://www.baidu.com", outPath,logoPath);
	}
	
	/**
	 * 读取二维码信息
	 * @throws Exception
	 */
	@Test
	public void testReadQRImg() throws Exception{
		//读取二维码信息
		String info =QRCodeUtils.readQRImg("C:/Users/yellowcong/Desktop/qr/test/15e2ff56-14be-4cf0-ad18-891e58152c42.png");
		System.out.println(info);
	}
	 
}
