package com.yellowcong.test;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:ApplicationContext.xml")
public class HibernateTest {
	private SessionFactory sessionFactory ;
	
	//自动注入
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Test
	public void testLoad(){
		Session session = this.sessionFactory.openSession();
		
		SQLQuery query  = session.createSQLQuery("show Tables ");
		
		List<Object> result = query.list();
		for(Object row:result){
			System.out.println(row.toString());
		}
	}
}
