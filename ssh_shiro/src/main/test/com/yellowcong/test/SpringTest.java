package com.yellowcong.test;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.yellowcong.dao.UserDao;
import com.yellowcong.entity.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:ApplicationContext.xml")
public class SpringTest {
	
	private UserDao userDao;

	@Resource
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
	
	@Test
	public void testAdds(){
		User user = new User();
		user.setAge(18);
		user.setNickname("test1");
		user.setPassword("test3");
		
		this.userDao.add(user);
	}
}
