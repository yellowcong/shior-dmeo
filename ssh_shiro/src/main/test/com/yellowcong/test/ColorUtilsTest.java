package com.yellowcong.test;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Random;

import org.junit.Test;

import com.yellowcong.test.ColorUtils.Colors;

public class ColorUtilsTest {
	
	@Test
	public void testRgbToHex(){
		//生成颜色  #FFE4E1
		String color = ColorUtils.convertRGBToHex(255,228,225);
		System.out.println(color);

		//生成颜色 16进制带补码操作 0xFFFFE4E1
		Color colorObj = new Color(255, 228, 225);
		String colorStr = ColorUtils.toHexFromColor(colorObj);
		System.out.println(colorStr);
	}
	
	/**
	 * 将16进制的转化为 rgb的
	 */
	@Test
	public void testHexToRgb(){
		//new Color(255, 228, 225);
		Color color = ColorUtils.toColorFromString("0xFFFFE4E1");
		System.out.println(color.getRed()+","+color.getGreen()+","+color.getBlue());
	}
	
	/**
	 * 将16进制的转化RGB
	 */
	@Test
	public void testHextoRgbStr(){
		String str = ColorUtils.toRgbFromString("0xFFFFE4E1");
		System.out.println(str);
	}
	
	@Test
	public void testRndColor(){
		Colors [] colArr = Colors.values();
		System.out.println(colArr.length);
		
		//获取到随机颜色的那个Enum字段
		Random random = new Random();
		int index= random.nextInt(colArr.length);
		System.out.println(colArr[index]);
		Colors color = colArr[index];
		System.out.println(color.getComent()+"\t"+color.getRgb()+color.getHex());
		
	}
	 
}
