package com.yellowcong.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class JDBCTest {
	
	private static final String DRIVE_NAME= "com.mysql.jdbc.Driver";
	private static final String DB_URL= "jdbc:mysql:///yellowcong";
	private static final String DB_USER= "root";
	private static final String DB_PASS= "root";
	
	public static void main(String [] args) throws Exception{
		
		//注册JDBC
		Class.forName(DRIVE_NAME);
		
		//获取连接
		Connection conn = DriverManager.getConnection(DB_URL, DB_USER,DB_PASS);
		
		//创建查询
		PreparedStatement ps = conn.prepareStatement("show tables ");
		
		//查询数据
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			System.out.println(rs.getString(1));
		}
		
		//关闭资源
		ps.close();
		conn.close();
		System.out.println("doubi");
	}
}
