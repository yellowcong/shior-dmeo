package com.yellowcong.service;

import com.yellowcong.entity.User;

public interface UserService {
	
	/**
	 * 添加用户
	 * @param user
	 */
	void add(User user);
	/**
	 * 删除用户
	 */
	void delete(int id);

}
