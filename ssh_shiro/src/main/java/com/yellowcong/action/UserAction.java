package com.yellowcong.action;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/user")
@Scope("prototype")
public class UserAction {
		
	
	@RequestMapping(value="/list",method=RequestMethod.GET)
	public String list(){
		return "user/list";
	}
	
	/**
	 * 用户登录列表
	 * @return
	 */
	@RequestMapping(value="/login",method=RequestMethod.GET)
	public String loginInput(){
		//shior获取当前的用户
		Subject currentUser = SecurityUtils.getSubject();
		
		//当用授权的情况下，直接跳转到主页
		if ( currentUser.isAuthenticated() ) {
			return "redirect:/user/list";
		}else{
			//直接
			return "user/login";
		}
	}
	@RequestMapping(value="/loginOut",method=RequestMethod.GET)
	public String loginOut(){
		Subject subject = SecurityUtils.getSubject();
		
		//直接退出session
		subject.logout();
		
		//获取到session ,这个Session是shior封装的
		Session session = subject.getSession();
		//跳转到登录
		return "redirect:/user/login";
	}
	
	@RequestMapping(value="/error",method=RequestMethod.GET)
	public String error(){
		return "user/error";
	}
	/**
	 * 用户登录列表
	 * @return
	 */
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public String login(String username,String password){
		try {
			//shior获取当前的用户
			Subject currentUser = SecurityUtils.getSubject();
			
			//当用户没有授权的情况下
			if ( !currentUser.isAuthenticated() ) {
				//用户登录
				UsernamePasswordToken token = new UsernamePasswordToken(username,password);
			    
				//保存
				token.setRememberMe(true);
			    currentUser.login(token);
			    //redirect 方法返回的是返回掉用的 url
			    return "redirect:/user/list";
			}
		} catch (Exception e) {
			throw new  UnknownAccountException(); 
		}
		return "user/list";
	}
}	
