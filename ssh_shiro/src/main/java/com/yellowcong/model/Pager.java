package com.yellowcong.model;

import java.util.List;

/**
 * 分页需要用的类
 * 
 * @author yellowcong
 * @date 2017年8月27日
 * @param <T>
 */
public class Pager<T> {
	/**
	 * 页面大小
	 */
	private int pageSize;
	
	/**
	 * 当前页
	 */
	private int pageNow;
	/**
	 * 页面条数
	 */
	private int pageCount;
	
	/**
	 * 多少条数据
	 */
	private int rowCount;
	
	/**
	 * 存储的数据
	 */
	private List<T> data;

	public int getPageSize() {
		return this.pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageNow() {
		return this.pageNow;
	}

	public void setPageNow(int pageNow) {
		this.pageNow = pageNow;
	}

	public int getPageCount() {
		return this.pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public int getRowCount() {
		return this.rowCount;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	public List<T> getData() {
		return this.data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}
}
