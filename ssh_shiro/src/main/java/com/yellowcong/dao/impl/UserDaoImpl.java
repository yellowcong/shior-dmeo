package com.yellowcong.dao.impl;

import org.springframework.stereotype.Repository;

import com.yellowcong.dao.UserDao;
import com.yellowcong.entity.User;

/**
 * 用户的Dao
 * @author yellowcong
 * @date 2017年8月27日
 */
@Repository("userDao")
public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao{
	
}
