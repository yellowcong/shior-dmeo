package com.yellowcong.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 创建日期:2017年9月23日 <br/>
 * 创建用户:yellowcong <br/>
 * 功能描述:用户控制成
 */
@Controller
@RequestMapping("/user")
@Scope("prototype")
public class UserController {
	
	/**
	 * 跳转到登录页面
	 * 创建日期:2017年9月23日<br/>
	 * 创建用户:yellowcong<br/>
	 * 功能描述:
	 * @return
	 */
	@RequestMapping(value="/login",method=RequestMethod.GET)
	public String loginInput(){
		return "user/loginInput";
	}
	
	/**
	 * 创建日期:2017年9月23日<br/>
	 * 创建用户:yellowcong<br/>
	 * 功能描述:用户登录操作
	 * @param username
	 * @param password
	 * @return
	 */
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public String login(String username,String password){
		Subject subject = SecurityUtils.getSubject();
		
		//判断用户是否授权
		if(!subject.isAuthenticated()){
			//用户登录
			UsernamePasswordToken token = new UsernamePasswordToken(username,password);
			//保存session
			token.setRememberMe(true);
			
			//登录用户
			subject.login(token);
			
			//redirect 方法返回的是
		    return "redirect:/user/index";
		}
		
		return "user/loginInput";
	}
	
	
	@RequestMapping("/list")
	public String list(){
		return "user/list";
	}

	@RequestMapping("/error")
	public String error(){
		return "user/error";
	}
}
