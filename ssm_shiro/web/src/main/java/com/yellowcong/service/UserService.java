package com.yellowcong.service;

import java.util.List;

import com.yellowcong.model.User;

/**
 * 创建日期:2017年9月23日 <br/>
 * 创建用户:yellowcong <br/>
 * 功能描述:  
 */
public interface UserService {
	
	/**
	 * 
	 * 创建日期:2017年9月23日<br/>
	 * 创建用户:yellowcong<br/>
	 * 功能描述:通过年龄，获取用户
	 * @return
	 */
	List<User> list();
	
	/**
	 * 获取用户条数
	 * 创建日期:2017年9月23日<br/>
	 * 创建用户:yellowcong<br/>
	 * 功能描述:
	 * @return
	 */
	Integer getCount();
	
	/**
	 * 用户登录
	 * 创建日期:2017年9月23日<br/>
	 * 创建用户:yellowcong<br/>
	 * 功能描述:
	 * @param username
	 * @return
	 */
	User login(String username);
	
}
