package com.yellowcong.test;

import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

import com.yellowcong.model.Role;


public class RoleTest {

	//这种方法用得最多
	//通过ClassPathApplication 来获取对象
	@Test
	public void testGetUser(){
		//使用ApplicationContext 来实例化对象的时候，会将对象实例化一份
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath*:spring-mybatis.xml");
		Role user = (Role)context.getBean("role");
		System.out.println(user.getUsername());
		
	}
	
	//通过BeanFactory 来实例化对象
	@Test
	public void getByFactory(){
		//实例化容器， 但是不会直接获取对象， 这个适用于小内存的移动设备
		BeanFactory factory = new XmlBeanFactory(new ClassPathResource("ApplicationConfig.xml"));
		//当我们调用的时候就会实例化对象
		Role user = (Role)factory.getBean("user");
		System.out.println(user.getUsername());
	}
	
	//通过FileSystemXmlApplicationContext获取
	@Test
	public void getByFilePath(){
		String url = Role.class.getClassLoader().getResource("spring.xml").toString();
		System.out.println(url);
		ApplicationContext context = new FileSystemXmlApplicationContext(url);
		
		Role user = (Role)context.getBean("user");
		System.out.println(user.getUsername());
	}
}
