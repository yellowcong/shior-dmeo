package com.yellowcong.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mysql.jdbc.PreparedStatement;
import com.yellowcong.model.User;
import com.yellowcong.service.UserService;

/**
 * 创建日期:2017年9月23日 <br/>
 * 创建用户:yellowcong <br/>
 * 功能描述:  
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:spring.xml")
public class TestUserService{
	
	UserService userService;
	
	@Resource(name="userService")
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	/**
	 * 
	 * 创建日期:2017年9月23日<br/>
	 * 创建用户:yellowcong<br/>
	 * 功能描述:测试条数
	 */
	@Test
	public void testGetCnt(){
		Integer cnt = this.userService.getCount();
		System.out.println(cnt);
	}
	
	/**
	 * 
	 * 创建日期:2017年9月23日<br/>
	 * 创建用户:yellowcong<br/>
	 * 功能描述:获取所有用户
	 */
	@Test
	public void testListAll(){
		//获取所有的用户数据
		List<User> users = this.userService.list();
		
		for(User user:users){
			System.out.println(user.getUsername());
		}
	}
}
